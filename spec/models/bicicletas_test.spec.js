var mongoose = require('mongoose')
var Bicicleta = require("../../models/bicicleta")

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser:true, useUnifiedTopology:true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection Error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            //Espera a que termine de ejecutarse para ir al siguiente test
            done()
        })
    })

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            mongoose.disconnect(err);
            done()
        })
    })

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1)
            expect(bici.color).toBe("verde")
            expect(bici.modelo).toBe("urbana")
            expect(bici.ubicacion[0]).toBe(-34.5)
            expect(bici.ubicacion[1]).toBe(-54.1)
        })
    })

    describe('Bicicleta.allBicis', ()=>{
        it('comienza vacia', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done()
            })
        })
    })

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done)=>{
            var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1)
                    expect(bicis[0].code).toEqual(1);
                    done()
                })
            })
        })
    })

    describe('Bicicleta.findByCode', ()=>{
        it('debe devolver la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color:"roja", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            if(err) console.log(err);
                            expect(targetBici.code).toBe(aBici.code),
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done()
                        })
                    })
                })
            })
        })
    })

    describe('Bicicleta.removeByCode', ()=>{
        it('debe eliminar la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color:"roja", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.removeByCode(1, function(err, targetBici){
                            if(err) console.log(err);
                            Bicicleta.allBicis(function(err, bicis){
                                if(err) console.log(err);
                                expect(bicis.length).toBe(1)
                                expect(bicis[0].code).toBe(aBici2.code)
                                done()
                            })
                        })
                    })
                })
            })
        })
    })
})

/*
Test unitarios antes de agregar la persistencia con MongoDB

//function escribir(){ console.log('testeando...') };
//Funcion para ejecutar codigo especifico antes de cada test
beforeEach(function(){
    console.log("´testeando…´");
    Bicicleta.allBicis = [];
})

//function escribir(){ console.log('testeando...') };  beforeEach( () => { escribir() });

//describe = grupo o agrupacion de testing
describe('Bicicleta.allBicis', ()=>{
    //it =  que es lo que se quiere probar
    it('comienza vacia', ()=>{
        //expect = es el test en sí
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicleta.add', ()=>{
    it('agregamos una Bici', ()=>{
        //Precondicion o estado previo
        expect(Bicicleta.allBicis.length).toBe(0)

        var a = new Bicicleta(1, 'rojo', 'urbana', [6.246, -75.6022])
        Bicicleta.add(a)

        //Postcondicion o estado posterior
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(a)
    })
})

describe('Bicicleta.findById', ()=>{
    it('Debe devolver la bici con id 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0)
        var aBici = new Bicicleta(1, 'verde', 'montaña')
        var aBici2 = new Bicicleta(2, 'rojo', 'urbana')
        Bicicleta.add(aBici)
        Bicicleta.add(aBici2)
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1)
        expect(targetBici.color).toBe(aBici.color)
        expect(targetBici.modelo).toBe(aBici.modelo)
    })
})

describe('Bicicleta.removeId', ()=>{
    it('Debe devolver la coleccion sin la bici con id 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0)
        var aBici = new Bicicleta(1, 'verde', 'montaña')
        var aBici2 = new Bicicleta(2, 'rojo', 'urbana')
        Bicicleta.add(aBici)
        Bicicleta.add(aBici2)
        Bicicleta.removeId(1);
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0].id).toBe(2)
    })
})*/