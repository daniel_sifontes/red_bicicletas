var Bicicleta = require("../../models/bicicleta")
var request = require('request')
var server = require('../../bin/www')

describe('Bicicleta API', ()=>{
    describe('GET Bicicletas /', ()=>{
        it('Status 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0)

            var a = new Bicicleta(1, 'negro', 'urbana', [6.246, -75.6022])
            Bicicleta.add(a)

            request.get('http://localhost:3000', (error, response, body)=>{
                expect(response.statusCode).toBe(200)
            })

        })
    })

    describe('POST Bicicletas /create', ()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type':'application/json'}
            var aBici= '{"id":10, "color":"rojo", "modelo":"urbana", "lat":6.2, "lng":"-75.6"}'

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body:aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                /*indica que termina el test con el done porque 
                sino termina el test sin esperar el resultado del request*/
                done();
            })

        })
    })

    describe('Update Bicicletas /:id/update', ()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type':'application/json'}
            var aBici= '{"id":10, "color":"rojo", "modelo":"urbana", "lat":6.2, "lng":"-75.6"}'

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body:aBici
            }, function(error, response, body){
                if (error){
                    console.log('No funciono el post para el update');
                }

                if(response.statusCode==200){
                    var aBici2= {"id":10, "color":"azulgrana", "modelo":"urbana", "lat":"6.2", "lng":"-75.6"}
                    request.put({
                        headers:headers,
                        url: "http://localhost:3000/api/bicicletas/"+aBici2.id+"/update",
                        body:aBici2,
                        json:true
                    }, function(error, response, body){
                        if (error) {
                            console.log(error)
                        }
                        expect(response.statusCode).toBe(200);
                        expect(Bicicleta.findById(10).color).toBe('azulgrana');
                        /*indica que termina el test con el done porque 
                        sino termina el test sin esperar el resultado del request*/
                        done();
                    })

                }
            })
        })
    })

    describe('Delete Bicicletas /delete', ()=>{
        it('Status 204', (done)=>{
            var headers = {'content-type':'application/json'}
            var aBici= '{"id":10, "color":"rojo", "modelo":"urbana", "lat":6.2, "lng":"-75.6"}'

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body:aBici
            }, function(error, response, body){
                if (error){
                    console.log('No funciono el post para el update');
                }

                if(response.statusCode==200){
                    var idDelete= {"id":10}
                    request.delete({
                        headers:headers,
                        url: "http://localhost:3000/api/bicicletas/delete",
                        body:idDelete,
                        json:true
                    }, function(error, response, body){
                        if (error) {
                            console.log(error)
                        }
                        expect(response.statusCode).toBe(204);
                        expect(Bicicleta.allBicis.length).toBe(0);
                        /*indica que termina el test con el done porque 
                        sino termina el test sin esperar el resultado del request*/
                        done();
                    })

                }
            })
        })
    })
    
})