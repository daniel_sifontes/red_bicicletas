var mongoose = require('mongoose');
var Reserva = require('./reserva')
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre:String
})

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({
        usuario:this._id,
        desde:this.desde,
        hasta:this.hasta
    })
    console.log(reserva)
    reserva.save(cb)
}

module.exports = mongoose.model('Usuario', usuarioSchema)